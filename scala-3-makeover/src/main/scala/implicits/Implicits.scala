package implicits

trait MakeImportant[T] {
  def escalate(thing: T): T
}

given stringMakeImportant as MakeImportant[String] {
  def escalate(thing: String) = thing.toUpperCase()
}

given intMakeImportant as MakeImportant[Int] {
  def escalate(thing: Int) = thing * 100
}

def doImportantWork[T](thing: T)(using imp: MakeImportant[T]) = {
  print("Doing important work with: ")
  println(imp.escalate(thing))
}

package extensions

def (s: String).times2() = s + s

extension StrExt on (s: String) {
  def times3() = s + s + s
  def times4() = s + s + s + s
}
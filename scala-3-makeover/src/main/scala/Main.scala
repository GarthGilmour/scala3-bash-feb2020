import shopping.Shop
import shopping.ItemID
import enumerations._
import movies.{greatActionMovies, Movie, Rating}
import macros._
import model._
import extensions._
import extensions.StrExt._
import implicits.{given _, _}

val menu = """
Pick an option:
  0) Exit
  1) Creator applications
  2) Extension methods
  3) Basic enumerations
  4) Enumerations as ADTs
  5) Export clauses
  6) Trait parameters
  7) Opaque type aliases
  8) Union types
  9) Intersection types
  10) Literal types
  11) Given instances
  12) Faking do...while loops
  13) Using macros
  14) Collections
  15) Quiz question
"""

@main def foobar(): Unit = {
  var notDone = true
  while(notDone) {
    println(menu)
    val choice = scala.io.StdIn.readInt()
    choice match {
      case 0 => notDone = false
      case 1 => creatorApplications()
      case 2 => extensionMethods()
      case 3 => basicEnumerations()
      case 4 => enumerationsAsADTs()
      case 5 => exportClauses()
      case 6 => traitParameters()
      case 7 => opaqueTypeAliases()
      case 8 => unionTypes()
      case 9 => intersectionTypes()
      case 10 => literalTypes()
      case 11 => givenInstances()
      case 12 => fakingDoWhile()
      case 13 => usingMacros()
      case 14 => collections()
      case 15 => question()
      case _ => println("Try again...")
    }
  }
}

def literalTypes() = {
  def specialFunction(input: "abc" | "def" | "ghi") = println(input)
  val specialNumber: 123 = 123
  val specialOption: Option[456] = Option(456)

  specialFunction("def")
  specialFunction("ghi")
  //specialFunction("xyz")  //Will not compile
  println(specialNumber)
  println(specialOption.getOrElse(0))
}

def usingMacros() = {
  println("Using macros")
  println(test2())
}

def fakingDoWhile() = {
  println("Output from first weird loop...")
  while({
    val x = Math.random()
    println(s"\t$x")
    x > 0.5
  })()

  println("Output from second weird loop...")
  while {
    val x = Math.random()
    println(s"\t$x")
    x > 0.5
  } do ()
}

def givenInstances() = { 
  doImportantWork("wibble")
  doImportantWork(123)
}

def question() = {
  import quiz.*

  *()
}

def intersectionTypes() = {
  def eatDrinkAndBeMerry(input: Edible & Drinkable) = {
    input.eat()
    input.drink()
  }
  //eatDrinkAndBeMerry(Apple()) //Will not compile
  //eatDrinkAndBeMerry(Milk())  //Will not compile
  eatDrinkAndBeMerry(Soup())
}

def unionTypes() = {
  def employ(name: String, reason: Degree | WorkExperience | Publication) = {
    print(s"Employing $name because of ")
    reason match {
      case Degree(subject) => println(s"their degree in $subject")
      case WorkExperience(years) => println(s"their $years years experience")
      case Publication(_) => println("they wrote a book")
    }
  }

  employ("Dave", Degree("Physics"))
  employ("Jane", WorkExperience(10))
  employ("Fred", Publication("ABC-123"))
}

def collections() = {
  def triplesOfQuotes(title: String) = {
    val movies = greatActionMovies()
    println(s"All possible triples of $title quotes")
    movies.find(_.title == title)
          .map(m => m.quotes.combinations(3))
          .fold(println("Not found!"))(_.foreach(println))
  }
  def movieTitlesByMonthReleased() = {
    val movies = greatActionMovies()
    println("Movie titles grouped by month of release")
    movies.groupMap(_.releaseDate.getMonth)(_.title)
          .foreach(row => {
            val(month, titles) = row
            print(s"\tMovies released in $month: ")
            titles.foreach(t => print(s" '$t'"))
            println()
          })
  }
  triplesOfQuotes("Commando")
  movieTitlesByMonthReleased()
}

def opaqueTypeAliases() = {
  val shop = new Shop()
  //shop.buy("AB12")  //Will Not Compile

  val item1: ItemID = ItemID("AB12")
  shop.buy(item1)
  shop.buy(ItemID("CD34"))
  shop.buy(ItemID("EF56"))

  println(shop)
}

def traitParameters() = {
  val pe = new MyPricingEngine()
  println(pe.calcDiscount(List("AB12","CD34")))
  println(pe.calcDiscount(List("AB12","CD34","EF56")))
}

def exportClauses() = {
  val buffer = new MyBuffer()
  buffer.append("Foo")
  buffer.append("Bar")
  buffer.append("Zed")
  buffer.putIn(3, "Wibble")
  buffer.putIn(12,"Wobble")
  println(buffer)
}

def enumerationsAsADTs() = {
  System.setProperty("testProp", "123")
  val result1 = readPropertyAsInt("testProp")
  val result2 = readPropertyAsInt("java.vendor")

  result1.fold(
    msg => println(s"Whoops - $msg"),
    num => println(s"Hurrah - $num")
  )

  result2.fold(
    msg => println(s"Whoops - $msg"),
    num => println(s"Hurrah - $num")
  )
}

def basicEnumerations() = {
  val d = Direction.NORTH
  print(d)
  d.printArrow()
}

def extensionMethods() = {
  val s1 = "Wibble"
  println(s1.times2())
  println(s1.times3())
  println(s1.times4())
}

def creatorApplications() = {
  val p1 = Person("Jane", 25)
  val p2 = Person("Dave")

  println(p1)
  println(p2)
}